package br.com.digio.androidtest

import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.data.remote.SafeResponse
import br.com.digio.androidtest.data.remote.repository.DigioEndpointRepository
import br.com.digio.androidtest.data.remote.safeRequest
import br.com.digio.androidtest.ui.feature.home.usecase.DigioEndpointUseCase
import br.com.digio.androidtest.ui.feature.home.viewmodel.HomeViewModel
import com.google.gson.Gson
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.junit.Assert
import kotlin.coroutines.CoroutineContext


fun withRobot(lb: DigioEndpointUseCaseRobot.() -> Unit): DigioEndpointUseCaseRobot =
    DigioEndpointUseCaseRobot().apply(lb)

class DigioEndpointUseCaseRobot {

    @MockK
    lateinit var mockRepositoryDigioEndPoint: DigioEndpointRepository

    private var dispatcher: CoroutineContext = Dispatchers.Unconfined + SupervisorJob()

    var digioEndpointUseCase: DigioEndpointUseCase

    var success: Boolean? = null

    var homeViewModel : HomeViewModel
    init {
        MockKAnnotations.init(this)
        digioEndpointUseCase = DigioEndpointUseCase(mockRepositoryDigioEndPoint)
        homeViewModel = HomeViewModel(digioEndpointUseCase,dispatcher)
    }

    var products: DigioProducts =
        Gson().fromJson(
            """
                {
                  "spotlight": [
                    {
                      "name": "Recarga",
                      "bannerURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/recharge_banner.png",
                      "description": "Agora ficou mais fácil colocar créditos no seu celular! A digio Store traz a facilidade de fazer recargas... direto pelo seu aplicativo, com toda segurança e praticidade que você procura."
                    }
                  ],
                  "products": [
                    {
                      "name": "XBOX",
                      "imageURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
                      "description": "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, programas de TV e muito mais!"
                    }
                  ],
                  "cash": {
                    "title": "digio Cash",
                    "bannerURL": "https://s3-sa-east-1.amazonaws.com/digio-exame/cash_banner.png",
                    "description": "Dinheiro na conta sem complicação. Transfira parte do limite do seu cartão para sua conta."
                  }
                }
            """.trimIndent(),
            DigioProducts::class.java
        )

    infix fun launch(lb: ActionDigioEndRobot.() -> Unit) =
        ActionDigioEndRobot(this).apply(lb)

    fun andWhenGetRepositoryDigioEndPointReturnsSuccess() {
        coEvery { mockRepositoryDigioEndPoint.getProducts() } returns products
    }

    fun andWhenGetDigioEndPointRepositoryRetunsError() {
        coEvery { mockRepositoryDigioEndPoint.getProducts()  }
    }
}

class ActionDigioEndRobot(private val robot: DigioEndpointUseCaseRobot) {

    infix fun check(lb: ResultDigioEndRobot.() -> Unit) =
        ResultDigioEndRobot(robot).apply(lb)


       fun getRepositoryDigioEndPoint() =

           coEvery {
               when (safeRequest { robot.digioEndpointUseCase.getProducts() }) {
                   is SafeResponse.Success -> {
                       robot.success = true
                   }
                   else -> {
                       robot.success = false
                   }

               }
           }

}

class ResultDigioEndRobot(private val robot: DigioEndpointUseCaseRobot) {

    fun thatGetRepositoryDigioEndPointSuccess() {
        Assert.assertEquals(true, robot.success)
    }
}