package br.com.digio.androidtest

import org.junit.Test

class DigioEndPointUseCaseTest {


    @Test
    fun `get repository  digioEndPoint with success`() {
        withRobot {
            andWhenGetRepositoryDigioEndPointReturnsSuccess()
        } launch {
            getRepositoryDigioEndPoint()
        } check {
            thatGetRepositoryDigioEndPointSuccess()
        }

    }

}