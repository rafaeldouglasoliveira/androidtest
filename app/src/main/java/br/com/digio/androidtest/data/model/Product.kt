package br.com.digio.androidtest.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val imageURL: String? = "",
    val name: String? = "",
    val description: String? = ""
) : Parcelable