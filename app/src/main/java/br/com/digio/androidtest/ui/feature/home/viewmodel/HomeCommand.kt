package br.com.digio.androidtest.ui.feature.home.viewmodel

import br.com.digio.androidtest.data.model.DigioProducts

sealed class HomeCommand {

    class Success(val digioProducts: DigioProducts) : HomeCommand()
    object Error : HomeCommand()
}
