package br.com.digio.androidtest

import android.app.Application
import br.com.digio.androidtest.di.DataModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class AndroidTestApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            // declare used Android context
            androidContext(this@AndroidTestApp)

            loadKoinModules(DataModule.loadDataModule())
        }
    }
}