package br.com.digio.androidtest.di

import br.com.digio.androidtest.data.remote.RetrofitServiceProvider
import br.com.digio.androidtest.data.remote.api.DigioEndpoint
import br.com.digio.androidtest.data.remote.repository.DigioEndpointRepository
import br.com.digio.androidtest.ui.feature.home.usecase.DigioEndpointUseCase
import br.com.digio.androidtest.ui.feature.home.viewmodel.HomeViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object DataModule {

    private val data = module {

        factory {
            RetrofitServiceProvider(androidApplication())
        }

        factory { get<RetrofitServiceProvider>().create(DigioEndpoint::class.java) }
        factory { DigioEndpointRepository(get()) }
        factory { DigioEndpointUseCase(get()) }

        viewModel {
            HomeViewModel(get())
        }

    }

    fun loadDataModule() = data
}