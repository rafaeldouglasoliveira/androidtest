package br.com.digio.androidtest.data.remote

import okhttp3.Interceptor
import okhttp3.Response

class BasicInterceptor  : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()
        val headerRequest = request.newBuilder()
            .build()
        return chain.proceed(headerRequest)
    }
}