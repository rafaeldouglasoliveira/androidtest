package br.com.digio.androidtest.ui.feature.home.usecase

import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.data.remote.repository.DigioEndpointRepository

class DigioEndpointUseCase(private val digioEndpointRepository: DigioEndpointRepository) {

    suspend fun getProducts(): DigioProducts {
        return digioEndpointRepository.getProducts()
    }
}