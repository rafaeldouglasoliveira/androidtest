package br.com.digio.androidtest.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Cash(
    val bannerURL: String? = "",
    val title: String? = ""
) : Parcelable