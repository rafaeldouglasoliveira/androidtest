package br.com.digio.androidtest.ui.feature.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.digio.androidtest.data.remote.SafeResponse
import br.com.digio.androidtest.data.remote.safeRequest
import br.com.digio.androidtest.ui.feature.home.usecase.DigioEndpointUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

    class HomeViewModel(private val digioEndpointUseCase: DigioEndpointUseCase,
                        private val dispatcher : CoroutineContext = Dispatchers.Main + SupervisorJob()
                    ) : ViewModel(){

    private val mutableHomeCommand = MutableLiveData<HomeCommand>()
    val command: LiveData<HomeCommand> = mutableHomeCommand

    fun getProducts() {

        viewModelScope.launch(dispatcher) {

           when(val response = safeRequest {digioEndpointUseCase.getProducts()  }) {

               is SafeResponse.Success -> {
                   HomeCommand.Success(response.value).run()
               }
               else -> {
                   HomeCommand.Error.run()
               }
           }
        }
    }

    private fun HomeCommand.run() {
        mutableHomeCommand.postValue(this)
    }

}