package br.com.digio.androidtest.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DigioProducts (
    val cash: Cash? = null,
    val products: List<Product>? = emptyList(),
    val spotlight: List<Spotlight>? = emptyList()
) : Parcelable