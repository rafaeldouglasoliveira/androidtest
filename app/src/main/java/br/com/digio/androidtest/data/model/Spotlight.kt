package br.com.digio.androidtest.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Spotlight(
    val bannerURL: String? = "",
    val name: String? = "",
    val description: String? = ""
) : Parcelable