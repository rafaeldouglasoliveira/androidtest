package br.com.digio.androidtest.data.remote.repository

import br.com.digio.androidtest.data.remote.api.DigioEndpoint

class DigioEndpointRepository(private val digioEndpoint : DigioEndpoint) {

    suspend fun getProducts() = digioEndpoint.getProducts()
}