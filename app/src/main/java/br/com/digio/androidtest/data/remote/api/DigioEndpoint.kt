package br.com.digio.androidtest.data.remote.api

import br.com.digio.androidtest.data.model.DigioProducts
import retrofit2.http.GET

interface DigioEndpoint {
    @GET("sandbox/products")
    suspend fun getProducts(): DigioProducts
}