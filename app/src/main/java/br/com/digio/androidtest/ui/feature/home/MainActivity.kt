package br.com.digio.androidtest.ui.feature.home

import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.ui.feature.home.adapter.ProductAdapter
import br.com.digio.androidtest.ui.feature.home.adapter.SpotlightAdapter
import br.com.digio.androidtest.ui.feature.home.viewmodel.HomeCommand
import br.com.digio.androidtest.ui.feature.home.viewmodel.HomeViewModel
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private lateinit var txtMainDigioCash: TextView
    private lateinit var recyMainProducts: RecyclerView
    private lateinit var recyMainSpotlight: RecyclerView
    private lateinit var body: ConstraintLayout
    private lateinit var loadDigioContainer: ConstraintLayout


    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }

    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    private val homeViewModel by inject<HomeViewModel>()

    override fun onResume() {
        super.onResume()
        setUpObservers()
        init()
        setupDigioCashText()
        getProducts()
    }

    private fun init() {
        txtMainDigioCash = findViewById(R.id.txtMainDigioCash)
        recyMainProducts = findViewById(R.id.recyMainProducts)
        recyMainSpotlight = findViewById(R.id.recyMainSpotlight)
        body = findViewById(R.id.body)
        loadDigioContainer = findViewById(R.id.loadDigioContainer)

        recyMainProducts.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyMainProducts.adapter = productAdapter

        recyMainSpotlight.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyMainSpotlight.adapter = spotlightAdapter
        body.visibility = View.GONE
        loadDigioContainer.visibility = View.VISIBLE
    }

    private fun setupDigioCashText() {
        val digioCacheText = "digio Cache"
        txtMainDigioCash.text = SpannableString(digioCacheText).apply {
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.blue_darker)
                ),
                0,
                5,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.font_color_digio_cash)
                ),
                6,
                digioCacheText.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }

    private fun setUpObservers() {
        homeViewModel.command.observe(this, Observer {
            when (it) {

                is HomeCommand.Success -> {
                    successProducts(it.digioProducts)
                }
                else -> {
                    errorProducts()
                }
            }
        })
    }

    private fun getProducts() {

        homeViewModel.getProducts()
    }

    private fun successProducts(digioProducts: DigioProducts) {
        loadDigioContainer.visibility = View.GONE
        body.visibility = View.VISIBLE

        digioProducts.products?.let {

            productAdapter.products = it
        }

        digioProducts.spotlight?.let {
            spotlightAdapter.spotlights = it
        }

    }

    private fun errorProducts() {

        val message = getString(R.string.error)

        loadDigioContainer.visibility = View.GONE
        body.visibility = View.GONE

        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
    }
}